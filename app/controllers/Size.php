<?php

namespace mvc\controllers;

use mvc\core\Controller;
use mvc\models\Size as SizeModel;

class Size extends Controller
{

    public $size;

    public function __construct()
    {
        $this->size = new SizeModel();
    }

    /**
     * add new category
     */

    public function add()
    {
        $this->size->setName($_POST['name']);
        $this->size->add();
        $this->redirect("Location: http://localhost/mvc/public/size/addform");

    }

    public function delete()
    {
        $this->size->setId($_POST['id']);
        $this->size->delete();

        $this->redirect("Location: http://localhost/mvc/public/size/list");
    }

    /**
     * Shows size list
     */
    public function showSize()
    {
        //size load
        $size = $this->size->load();
        //layout render
        $this->view('size/list', $data = ['size' => $size]);
    }

    /**
     * Shows add size form
     */
    public function sizeform()
    {
        //size load
        $size = $this->size->load();

        //layout render
        $this->view('size/addform', $data = ['size' => $size]);

    }

}
