<?php

namespace mvc\controllers;

use mvc\core\Controller;
use mvc\models\Colour as ColourModel;

class Colour extends Controller
{

    public $colour;

    public function __construct()
    {
        $this->colour = new ColourModel();
    }

    /**
     * add new category
     */

    public function add()
    {
        $this->colour->setName($_POST['name']);
        $this->colour->add();
        $this->redirect("Location: http://localhost/mvc/public/colour/addform");

    }

    public function delete()
    {
        $this->colour->setId($_POST['id']);
        $this->colour->delete();

        $this->redirect("Location: http://localhost/mvc/public/colour/list");
    }

    /**
     * Shows category list
     */
    public function showColour()
    {
        //category load
        $colour = $this->colour->load();
        //layout render
        $this->view('colour/list', $data = ['colour' => $colour]);
    }

    /**
     * Shows add colours form
     */
    public function colourform()
    {
        $this->view('colour/addform');
    }

}
