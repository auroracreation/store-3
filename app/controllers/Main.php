<?php

namespace mvc\controllers;

use mvc\core\Controller;

class Main extends Controller
{
    public function main()
    {
        $this->view('main');
    }
}
