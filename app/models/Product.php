<?php

namespace mvc\models;

class Product
{

    /**
     * @var int
     */
    public $id;
    public $name;
    public $category;
    public $size;
    public $colour;
    public $description;
    public $price;
    /**
     * Product constructor.
     */
    public function __construct()
    {
        $this->database = \mvc\core\Database::getInstance();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param string $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return string
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param string $size
     */
    public function setSize($size)
    {
        $this->size = $size;
    }

    /**
     * @return string
     */
    public function getColour()
    {
        return $this->colour;
    }
    /**
     * @param string $colour
     */
    public function setColour($colour)
    {
        $this->colour = $colour;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }





    /**
     * Add product to database
     * @param $product
     */
    public function add()
    {
        $this->database->insertRow('product', "( `name`, `category`, `size`, `colour`,`description`,`price`) VALUES(?,?,?,?,?,?)", [$this->name, $this->category, $this->size, $this->colour, $this->description, $this->price]);
    }

    /**
     * Delete product from database
     * @param $product
     */

    public function delete()
    {
        $this->database->deleteRow('product', 'WHERE id = ?', [$this->id]);
    }

    /**
     *load rows from table
     */

    public function load()
    {
        $result = $this->database->getRows('*', 'product');

        return $result;
    }

    /**
     *load rows from table by id
     */

    public function loadByCategoryId($id)
    {
        $result = $this->database->getRows('*', 'product', "WHERE `category` = '$id' " );

        return $result;
    }


}