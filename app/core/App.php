<?php

use mvc\controllers\Register;

namespace mvc\core;
/*
 * Class App
 */
class App extends Controller
{
    /**
     * @var controller
     */
    protected $controller = 'home';
    /**
     * @var method
     */
    protected $method = 'index';
    /**
     * @var params
     */
    protected $params = [];
    /**
     * Main clontrollers namespace
     * @var namespace
     */
    private $namespace = "\\mvc\\controllers\\";

    /**
     * Constructor
     */
    public function __construct()
    {
        if (!isset($_GET['url'])) {
            $_GET['url'] = 'home/index';
        }
        $url = $this->parseUrl($_GET['url']);
        $nameClass = $this->parseNamespace($url);
        $size = sizeof($nameClass);
        if ($url != null) {
            $this->controller = ucfirst($nameClass[$size - 1]);
            for ($i = 0; $i <= ($size - 2); $i++) {
                $result = ucfirst($nameClass[$i]) . "\\";
                $this->namespace = $this->namespace . $result;
            }
            $this->controller = $this->namespace . $this->controller;
            $this->controller = new $this->controller();
            $this->method = $url[1];
            $this->params = $url ? array_values($url) : [];
            call_user_func_array([$this->controller, $this->method], $this->params);
        } else {
            $this->controller = $this->namespace . $this->controller;
            $this->controller = new $this->controller();
            call_user_func_array([$this->controller, $this->method], $this->params);
        }
    }

    /**
     * Parse subfolder and controller name
     * @param array $url
     * @return array
     */
    public function parseNamespace($url)
    {
        if (isset($url[0])) {
            return $nameClass = explode('_', filter_var(rtrim($url[0], '_'), FILTER_SANITIZE_URL));
        }
    }

    /**
     * Parse URL into array elements
     * @return array
     */
    public function parseUrl($uri)
    {
        if (isset($uri)) {
            return $url = explode('/', filter_var(rtrim($uri, '/'), FILTER_SANITIZE_URL));
        }
    }
}