<?php

namespace mvc\controllers;

use mvc\core\Controller;
use mvc\models\Category as CategoryModel;
use mvc\models\Product as ProductModel;


class Index extends Controller
{
    public function __construct()
    {
        $this->category = new CategoryModel();
        $this->product = new ProductModel();
    }

        /**
         * Shows index
         */

        public function Index()
        {
            $category = $this->category->load();
            $product = $this->product->load();

            $this->header();

            $this->view('store/index', $data = ['category' => $category, 'product' => $product]);
        }


}

